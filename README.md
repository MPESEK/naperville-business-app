This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## Overview

This project allows a user to enter a type of business as well as how many results they want to
 search for. Upon submitting this information the app makes a call to Yelp's API to display the 
 search results, and creates links on the main page to access another page that has more information
for that search query.

To Load the project you can do npm init to get the dependancies in our package.json. Then npm start opens our web app. Additionally, to run server locally move into src directory and enter 'node server.js'.

Additional project ideas:
Can store user selected favorites and/or previous searches in a database

(can also do this temporarily with local storage in the same way I stored props in the second page)
and allow it to be displayed on another page or through a modal/collapse type element on main page

Can allow user to print/export to pdf

Incorporate another api for directions to that business

## Directory Structure

In SRC we have components and redux folders, a shared folder with baseUrl we are using, and the rest is boiler plate create-react-app scaffolding.

## Dependancies
"@testing-library/jest-dom": "^4.2.4",
    "@testing-library/react": "^9.3.2",
    "@testing-library/user-event": "^7.1.2",
    "body-parser": "^1.19.0",
    "bootstrap": "^4.4.1",
    "cors": "^2.8.5",
    "dotenv": "^8.2.0",
    "express": "^4.17.1",
    "local-storage": "^2.0.0",
    "react": "^16.12.0",
    "react-dom": "^16.12.0",
    "react-redux": "^7.1.3",
    "react-redux-form": "^1.16.14",
    "react-router-dom": "^5.1.2",
    "react-scripts": "3.4.0",
    "reactstrap": "^8.4.1",
    "redux": "^4.0.5",
    "redux-logger": "^3.0.6",
    "redux-thunk": "^2.3.0",
    "yelp-fusion": "^3.0.0"



