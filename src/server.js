const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const dotenv = require('dotenv')
const app = express();
const port = process.env.PORT || 5000;
const yelp = require('yelp-fusion');
const searchRequest = {
    term: 'restaurants',
    location: 'Naperville Illinois',
    limit: 10,
  };
dotenv.config()

const client = yelp.client(process.env.API_KEY);
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send({ express: 'Hello From Express' });
});
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*'); // Maybe not needed with yelp-fusion, but handles proxy for apis that throw cors errors
    next();
  });
app.post('/bizzSearch', (req, res) => { // endpoints for our local server that sends our information to Yelp through their client.search functionality
    console.log("req body is", req.body)
    searchRequest.term = req.body.search
    if(req.body.limit){
      searchRequest.limit = req.body.limit
    }
    else{
      searchRequest.limit = 10
    }
    
    console.log("updated search terms", searchRequest)

    client.search(searchRequest)
    .then((response) => {             
      console.log("response is",response);
      res.json(JSON.parse(response.body)); // parse the response 
    })
    .catch((error) => {
      console.log(error);
    });
});

app.listen(port, () => console.log(`Listening on port ${port}`));