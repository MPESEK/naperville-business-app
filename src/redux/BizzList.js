import * as ActionTypes from './ActionTypes';

export const BizzList = (state = {
        errMess: null,
        bizzList: []
    }, action) => {
    switch(action.type) {
        case ActionTypes.ADD_BIZZ:
            return {...state, errMess: null, bizzList: action.payload};
        case ActionTypes.SEARCH_FAILED:
            return {...state, errMess: action.payload, bizzList: []};

        default:
            return state;
    }
}