import * as ActionTypes from './ActionTypes';
import { baseUrl } from '../shared/baseUrl';


export const searchBizz = (search,limit) => (dispatch) => {  // action creator using thunk to call other actions within
    console.log("limit is", limit)
    const newData = {
        search: search,
        location: "Naperville Illinois",
        limit: limit
    }
    console.log("newData is", newData)
    return fetch(baseUrl + 'bizzSearch', {
        method: "POST",
        body: JSON.stringify(newData),
        headers: {
          "Content-Type": "application/json"
        },
        credentials: "same-origin"
    })
        .then(response => {
            if (response.ok) {
                return response;
            }
            else {
                var error = new Error('Error ' + response.status + ': ' + response.statusText);
                error.response = response;
                throw error;
            }
        },
        error => {
            var errmess = new Error(error.message);
            throw errmess;
        })
        .then(response => response.json())
        .then(bizzList => dispatch(addBizz(bizzList))) // this populates our store after we get our JSON data
        .catch(error => dispatch(searchFailed(error.message))); 
       
}
export const addBizz = (bizzList) => ({
    type: ActionTypes.ADD_BIZZ,
    payload: bizzList
});
export const searchFailed = (errmess) => ({
    type: ActionTypes.SEARCH_FAILED,
    payload: errmess
});