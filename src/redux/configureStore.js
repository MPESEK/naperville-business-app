import { createStore, combineReducers, applyMiddleware } from 'redux';
import { BizzList } from './BizzList';

import thunk from 'redux-thunk';
import logger from 'redux-logger';


export const ConfigureStore = () => {
    const store = createStore(
        combineReducers({
            BizzList: BizzList,
        }),
        applyMiddleware(thunk, logger) // thunk for dispatching actions within our actioncreators, logger to see states in console after actions
    );

    return store;
}