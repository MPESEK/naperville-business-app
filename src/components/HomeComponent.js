import React, { Component } from 'react';
import { Button, Card,  CardTitle, Row, Col, Label,Jumbotron } from 'reactstrap';
import { Control, LocalForm } from 'react-redux-form';
import { Link } from 'react-router-dom';

function RenderBizz({ bizz}) { // creates the cards containing a link to business information page
    return(
        <Card>
            <Link to={`/business/${bizz.id}`} >          
                    <CardTitle>{bizz.name}</CardTitle>               
            </Link>
        </Card>
    );
}
class Home extends Component {
    constructor(props) {
        super(props);
        
        this.handleSubmit = this.handleSubmit.bind(this)
        this.state = {
            searchBox: null,
          };   
    }
    handleSubmit(values) { 
        this.props.searchBizz(values.search,values.limit); // submit button passes our limit and search parameters to api call
    }
    render(){
        console.log("type of bizzList",  this.props.bizzList.bizzList)
        let businesses = []
        if(this.props.bizzList.bizzList.businesses){   // if our businesses have not been populated we can't iterate over them
            businesses = this.props.bizzList.bizzList.businesses
        }
        const bizzList = businesses.map((bizz) => { // for every search result we will render a card with a link to the second page
            return (
                <div key={bizz.id} className="col-12 col-md-5 m-1">
                   <RenderBizz bizz={bizz} />
                </div>
            );
        });    
        return ( // using reactstrap Jumbotron and a simple form for main page, form has a onSubmit function that will call our action
            <div className="container">
                <div className="row">
                    <div className="col-12">
                    <Jumbotron fluid>
                    <div className="container">
                    <div className="row row-header">
                        <div className="col-12 col-sm-6">
                            <h1>Naperville Yelp Business Search</h1>
                            <p> Enter the number of results you want and the Name/Type of business!</p>
                        </div>
                    </div>
                </div>
                </Jumbotron> 
                    <LocalForm onSubmit={(values) => this.handleSubmit(values)}> 
                        <Row className="form-group col-12 col-md-6">
                            <Col>
                            <Label htmlFor="rating">Number of results</Label>
                            <Control.select model=".limit" id="limit" className="form-control">
                                <option>10</option>
                                <option>20</option>
                                <option>30</option>
                                <option>40</option>
                                <option>50</option>
                            </Control.select>
                            </Col>
                        </Row>
                        <Row className="form-group col-12 col-md-7">
                            <Col>
                            <Label htmlFor="search">Type of Business</Label>
                            <Control.textarea model=".search" id="search"
                                        rows="1" className="form-control" />
                            </Col>  
                            <Button type="submit" size="lg" className="bg-danger">
                                Submit
                            </Button> 
                        </Row>
                    </LocalForm>
                    </div>
                </div>
                <div className="row row-content">
                    {bizzList}
                </div>
            </div>
        );
        
}
}

    

    export default Home;