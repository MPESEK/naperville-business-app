import React, { Component } from 'react';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import Home from './HomeComponent';
import BizzDetail from './BizzDetail';
import {searchBizz} from '../redux/ActionCreators';
import { connect } from 'react-redux';

const mapDispatchToProps = dispatch => ({
  searchBizz: (search,limit) => { dispatch(searchBizz(search,limit))},
});
  const mapStateToProps = state => {
    return {
      bizzList: state.BizzList
    }
}

class Main extends Component {   
    render() {
        const bizzWithId = ({match}) => {   // match is our businessID in the url we are sending for 2nd page, we can filter our businesses that match this value
            console.log("with id bizzlist", this.props)
            var businesses = []
            if(this.props.bizzList.bizzList.businesses){ 
                businesses = this.props.bizzList.bizzList.businesses
            }
            return(
              <BizzDetail bizz={businesses.filter((bizz) => bizz.id === match.params.businessId)[0]}
                />
            );
          }
        return (                      
            <div> 
                <Switch>             
                    <Route path="/home" component={() => <Home bizzList={this.props.bizzList} searchBizz={this.props.searchBizz} />} />
                    <Route path="/business/:businessId" component={bizzWithId} />
                    <Redirect to="/home" />
                </Switch>      
        </div>
        )    
    }
}

export default withRouter(connect(mapStateToProps,mapDispatchToProps)(Main));