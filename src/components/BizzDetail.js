import React from 'react';
import { Card, CardImg,  CardText, CardBody,
   Breadcrumb, BreadcrumbItem, } from 'reactstrap';
import { Link } from 'react-router-dom';

function RenderBizz({bizz}) { // renders the info for our business that we received from yelp
    return(
        <div className="col-12 col-md-7 m-1">
                <Card>
                <CardImg top src={bizz.image_url} alt={bizz.name} id="myobj"/>
                    <CardBody>
                        <CardText id="my_text">Rating: {bizz.rating}<br></br> Review count: {bizz.review_count}<br></br> Phone: {bizz.phone} <br></br>
                                  URL: <a href ={bizz.url}>{bizz.url}</a> </CardText>
                    </CardBody>
                </Card>
        </div>
    );                  
}
const BizzDetail = (props) => {
    let bizz = null
    if(props.bizz){         // if we reloaded the page and no longer have props we can get them back from local storage
                            // if we were using a database we could take advantage of a method like componentDidMount and retrieve our selection
                            // but this seemed like an ok way to do it for this simplified project
                    
        const serializedState = JSON.stringify(props)
        localStorage.setItem('bizz', serializedState)    
    }
    else if(!props.bizz){ // because making something dissappear isn't enough; you have to bring it back
        bizz = JSON.parse(localStorage.getItem('bizz')) 
    }
    if(props.bizz) // two returns based on if we are using props or our bizz variable we got from local storage
    {
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/Home'>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{props.bizz.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{props.bizz.name}</h3>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <RenderBizz bizz={props.bizz}/>  
                </div>
            </div>
        );
    }
    else{
        return (
            <div className="container">
                <div className="row">
                    <Breadcrumb>
                        <BreadcrumbItem><Link to='/Home'>Home</Link></BreadcrumbItem>
                        <BreadcrumbItem active>{bizz.bizz.name}</BreadcrumbItem>
                    </Breadcrumb>
                    <div className="col-12">
                        <h3>{bizz.bizz.name}</h3>
                        <hr />
                    </div>
                </div>
                <div className="row">
                    <RenderBizz bizz={bizz.bizz}/>
                </div>
            </div>
        );
    }       
}
export default BizzDetail;